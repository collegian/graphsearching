package dfs;

import java.util.List;
import java.util.Map;

import graph.AdjacencyList;
import graph.components.Vertex;

public class DFS<T>
{
 private AdjacencyList<T> adjacencyList;
    
    public DFS(AdjacencyList<T> adjacencyList)
    {
	this.adjacencyList = adjacencyList;
    }
    
    public void depthFirstSearch(Vertex<T> source)
    {
	if(source==null || adjacencyList==null)
	    return;
	
	Map<Vertex<T>,List<Vertex<T>>> adjacentVertices = adjacencyList.getAdjacencyList();
	if(adjacentVertices.isEmpty() || !adjacentVertices.containsKey(source))
	{
	    return;
	}
	
	  List<Vertex<T>> vertices =  adjacentVertices.get(source);
	  for(Vertex<T> vertex : vertices)
	  {
	      if(!vertex.isVisited())
	      {
		  vertex.setParent(source);
		  vertex.setDistance(source.getDistance()+1);
		  vertex.setVisited(true);
		  depthFirstSearch(vertex);
	      }
	  }
    }
}
