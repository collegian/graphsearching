package bfs.queue;

import bfs.queue.linkedlist.DoubleLinkedList;
import bfs.queue.linkedlist.DoubleLinkedListImpl;

public class QueueImpl<T> implements Queue<T> 
{
        DoubleLinkedList<T> dll;
        
        public QueueImpl()
        {
            dll = new DoubleLinkedListImpl<>();
        }
        
	@Override
	public boolean offer(T element) 
	{
	    dll.add(element);
	    return true;
	}

	@Override
	public T poll() 
	{
	    return dll.remove();
	}

	public T peek()
	{
	  return dll.getCurrent();
	}
}
