package bfs.queue.linkedlist;

public interface DoubleLinkedList<T> 
{
   public void add(T element);
   
   public T remove();
   
   public boolean addAtIndex(T element, int index);
   
   public T removeAtIndex(int index);
   
   public T getCurrent();
   
   public int size();
}
