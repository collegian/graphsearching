package bfs.queue.linkedlist;

public class DoubleLinkedListImpl<T> implements DoubleLinkedList<T>
{
      private Node<T> root;
      private Node<T> current;
      private int size = 0;

	@Override
	public void add(T element) 
	{
          Node<T> node = new Node<>(element);
          size++;
          
          if(size==1)
          {
              root=node;
              current = node;
              return;
          }
          
          if(current!=null)
          {
              current.setNext(node);
              node.setPrevious(current);
          }
          
          current = node;
	}

	@Override
	public T remove() 
	{
	    if(current==null)
		return null;
	    
	    Node<T> previous = current.getPrevious(); 
	    previous.setNext(null);
	    Node<T> returnElement = current;
	    current = previous;
	    size--;
	    
	    return returnElement.getData();
	}

	@Override
	public boolean addAtIndex(T element, int index) 
	{
	   if(index<0 || index>size)
	   {
	       return false;
	   }
	   
	   if(index==size)
	   {
	       add(element);
	       return true;
	   }
	   
	   if(index==0)
	   {
	       Node<T> node = new Node<>(element);
	       root.setPrevious(node);
	       node.setNext(root);
	       root = node;
	       size++;
	       return true;
	   }
	   
           int counter = 1;
           Node<T> next = root.getNext();
           
           while(counter!=index)
           {
               next = next.getNext();
           }
           
           Node<T> node = new Node<>(element);
           Node<T> previous  = next.getPrevious();
           previous.setNext(node);
	   next.setPrevious(node);
	   node.setNext(next);
	   size++;
	   return true;
	}

	@Override
	public T removeAtIndex(int index) 
	{
           if(index<0 || index>=size)
               return null;
           
           if(index==0)
           {
               Node<T> next = root.getNext();
               next.setPrevious(null);
               Node<T> returnElement = root;
               root = next;
               size--;
               return returnElement.getData();
           }
           
           if(index==size-1)
           {
               Node<T> previous = current.getPrevious();
               previous.setNext(null);
               Node<T> returnElement = root;
               current = previous;
               size--;
               return returnElement.getData();
           }
           
           int counter = 1;
           Node<T> next = root.getNext();
           
           while(counter!=index)
           {
               next = next.getNext();
           }
           
           Node<T> previous = next.getPrevious();
           Node<T> nextOfCurrent = next.getNext();
           previous.setNext(nextOfCurrent);
           nextOfCurrent.setPrevious(previous);
           Node<T> returnElement = next;
           next=null;
           size--;
           
           return returnElement.getData();
	}

	@Override
	public int size() 
	{
		return size;
	}

	@Override
	public T getCurrent() 
	{
	    if(current==null)
	    return null;
	    
	    return current.getData();
	}
}
