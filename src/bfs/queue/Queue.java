package bfs.queue;

public interface Queue<T>
{
   public boolean offer(T element);
   
   public T poll();
   
   public T peek();
}
