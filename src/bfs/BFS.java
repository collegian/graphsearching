package bfs;

import java.util.List;
import java.util.Map;

import bfs.queue.Queue;
import bfs.queue.QueueImpl;
import graph.AdjacencyList;
import graph.components.Vertex;

public class BFS<T> 
{
    private AdjacencyList<T> adjacencyList;
    
    public BFS(AdjacencyList<T> adjacencyList)
    {
	this.adjacencyList = adjacencyList;
    }
    
    public void breadthFirstSearch(Vertex<T> source)
    {
	if(source==null || adjacencyList==null)
	    return;
	
	Map<Vertex<T>,List<Vertex<T>>> adjacentVertices = adjacencyList.getAdjacencyList();
	if(adjacentVertices.isEmpty() || !adjacentVertices.containsKey(source))
	{
	    return;
	}
	
	Queue<Vertex<T>> queue= new QueueImpl<>();
	queue.offer(source);
	
	Vertex<T> vertex = queue.poll();
	while(vertex!=null)
	{
	    List<Vertex<T>> vertices =  adjacentVertices.get(vertex);
	    for(Vertex<T> v: vertices)
	    {
		if(!v.isVisited())
		{
		    queue.offer(v);
		    v.setParent(vertex);
		    v.setDistance(vertex.getDistance()+1);
		}
	    }
	    vertex.setVisited(true);
	    vertex = queue.poll();
	}
    }
}
