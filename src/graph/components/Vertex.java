package graph.components;

public class Vertex<T> 
{
    private T vertex;
    private Vertex<T> parent;
    private int distance;
    private boolean visited;
    
    public static <T> Vertex<T> createVertex(T name)
    {
  	   Vertex<T> v = new Vertex<>(name);
  	   v.setDistance(0);
  	   return v;
    }
    
    private Vertex(T name)
    {
  	  this.vertex=name; 
    }
    
    public T getVertex()
    {
    	return vertex;
    }

    public Vertex<T> getParent() 
    {
	return parent;
    }

    public void setParent(Vertex<T> parent) 
    {
	this.parent = parent;
    }

    public int getDistance() 
    {
	return distance;
    }

    public void setDistance(int distance) 
    {
	this.distance = distance;
    }

    public boolean isVisited() {
	return visited;
    }

    public void setVisited(boolean visited) {
	this.visited = visited;
    }
}
