package graph;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import graph.components.Vertex;

public class AdjacencyList<T> 
{
   private ImmutableMap<Vertex<T>,List<Vertex<T>>> adjacentVertices;
   
   public static <T> AdjacencyList<T> createAdjacencyList(Map<Vertex<T>,List<Vertex<T>>> adjacencyList)
   {
	   AdjacencyList<T> adjacentVertices = new AdjacencyList<>(adjacencyList);
 	   return adjacentVertices;
   }
   
   private AdjacencyList(Map<Vertex<T>,List<Vertex<T>>> adjacencyList)
   {
 	  this.adjacentVertices=ImmutableMap.copyOf(adjacencyList); 
   }
   
   public Map<Vertex<T>,List<Vertex<T>>> getAdjacencyList()
   {
   	return adjacentVertices;
   }
}
